Rails.application.routes.draw do
  root to: 'parser#index', via: 'get'
  match 'brandlist', to: 'parser#brandlist', via: 'post'
  match 'phonelist', to: 'parser#phonelist', via: 'post'
  match 'searchphone', to: 'parser#searchphone', via: 'post'
end
