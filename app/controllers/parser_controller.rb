class ParserController < ApplicationController
  require 'test_parser.rb'

### Главная страница. Парсинг списка брендов ###

  def index
    parser = TestParser.new()
    @brand_list = parser.parseBrandList
  end

### Парсинг списка моделей выбранного бренда ###

  def brandlist
    link = params[:brand_select]
    unless link == ''
        parser = TestParser.new()
        parser.open(link)
        @phone_list = parser.parsePhoneList
        respond_to do |format|
            format.js { }
        end
    end
  end

### Парсинг информации о телефоне ###

  def phonelist
    plink = params[:phone_select]
    unless plink == ''
        parser = TestParser.new()
        parser.open(plink)
        @name = parser.getName
        @reslist = parser.parsePhoneInfo
        respond_to do |format|
            format.js { }
        end          
    end
  end

### Попытка найти и распарсить страницу телефона по наименованию ###

  def searchphone
    slink = params[:search_phone]
    @mytext = 'something'
    unless slink == ''
        slink = 'results.php3?sQuickSearch=yes&sName=' + slink
        slink = slink.gsub(" ","+")
        parser = TestParser.new()
        parser.open(slink)
        first = parser.searchPhone
        parser.open(first)
        @name = parser.getName
        @reslist = parser.parsePhoneInfo
        respond_to do |format|
            format.js { }
        end                  
    end
  end

end
