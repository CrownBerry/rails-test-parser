function brandCall() {
  $.ajax({
      type: "POST",
      url: "/brandlist",
      async: true,
      dataType: 'script',
      data: $("form").serialize()
    });
}

function phoneCall() {
  $.ajax({
      type: "POST",
      url: "/phonelist",
      async: true,
      dataType: 'script',
      data: $("form").serialize()
    });
}

function phoneTryFind() {
  $.ajax({
      type: "POST",
      url: "/searchphone",
      async: true,
      dataType: 'script',
      data: $("#f-three").serialize()
    });
}
