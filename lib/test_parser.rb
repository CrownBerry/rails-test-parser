class TestParser
    require 'mechanize'
    require 'nokogiri'

### Инициализация, выбор сайта для парсинга ###
### По умолчанию берется сайт www.gsmarena.com ###

    def initialize(site = 'www.gsmarena.com')
        @site = site
        url = 'http://' + site + '/'
        @agent = Mechanize.new
        @page = @agent.get(url)
    end

### Загрузить страницу для парсинга ###

    def open(link = '')
        url = 'http://' + @site + '/' + link 
        @page = @agent.get(url)
    end

### Парсинг всех страниц моделей по бренду ###

    def parsePhoneList()
        @plist = Array.new
        recursiveParse
        return @plist
    end

### Парсинг списка моделей с главной страницы ###

    def parseBrandList()
        brand_list = @page.search('.brandmenu-v2').search('ul')
        list = Array.new() 
        brand_list.search('li').map {|l| list << [l.text, l.search('a')[0]['href']] }
        return list
    end

### Парсинг страницы телефона, выгрузка спецификаций ###

    def parsePhoneInfo()
        spec = @page.search('#specs-list')
        aggregate = []   
        spec.search('table').map { |t| aggregate << parseTable(t) }
        return aggregate
    end

### Парсинг страницы с результатом поиска, выбор первого предложенного варианта ###

    def searchPhone()
        makers = @page.search('.makers ul li a')[0]['href']
    end

### Парсинг имени модели телефона с его страницы ###

    def getName()
        name = @page.search('.specs-phone-name-title').text
    end

    private

    ### Рекурсивный парсинг страниц с моделями бренда, по пагинации ###

      def recursiveParse
        phone_list = @page.search('.makers').search('ul')
        phone_list.search('li').map { |l| @plist << [l.text, l.search('a')[0]['href']] }        
        link = @page.link_with(:class => 'pages-next')
        unless link.nil?
            @page = link.click
            recursiveParse()
        end
      end

    ### Парсинг таблицы спецификации ###

      def parseTable(tab)
        block = {name: nil, inner: []}
        tab.search('tr').map do |r| 
            block[:name] ||= r.search('th').text
            block[:inner] << r.search('td').map { |d| d.text }
        end
        return block
      end
end